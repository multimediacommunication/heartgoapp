﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace HeartGo.Bluetooth
{
    
    [Serializable]
    public class MessageObject
    {

        public MessageObject()
        {
            Timestamp = DateTime.Now;
        }

        public DateTime Timestamp { get; set; }
        public string Message { get; set; }


    }

    [Serializable]
    public class MessageObject<T> : MessageObject
    {

        public T Data { get; set; }

    }
}