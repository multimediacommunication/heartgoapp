﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using HeartGo.Activities;

namespace HeartGo.Bluetooth
{
    public class MessageHandler : Handler
    {
       
        public MessageHandler()
        {

        }

        public override void HandleMessage(Message msg)
        {
            try
            {
                switch (msg.What)
                {
                    case Constants.MESSAGE_STATE_CHANGE:
                        {
                            var text2 = $"";
                            switch (msg.What)
                            {
                                case BluetoothService.STATE_CONNECTED:
                                    text2 = "STATE_CONNECTED";
                                    break;
                                case BluetoothService.STATE_CONNECTING:
                                    text2 = "STATE_CONNECTING";
                                    break;
                                case BluetoothService.STATE_LISTEN:
                                    text2 = "STATE_LISTEN";
                                    break;
                                case BluetoothService.STATE_NONE:
                                    text2 = "STATE_NONE";
                                    break;
                            }

                            var text = $"MESSAGE_STATE_CHANGE {text2}";
                            MyApp.Instance.CurrActivity.ShowShortToast(text);
                            Log.Info(MyApp.TAG, text);
                            break;
                        }
                    case Constants.MESSAGE_WRITE:
                        {
                            var writeBuffer = (byte[])msg.Obj;
                            //var writeMessage = Encoding.ASCII.GetString(writeBuffer);

                            //var text = $"MESSAGE_WRITE {writeMessage}";
                            //MyApp.Instance.CurrActivity.ShowShortToast(text);
                            //Log.Info(MyApp.TAG, text);

                            var mo = (MessageObject)MyApp.Instance.BtService.ByteArrayToObject(writeBuffer);
                            
                            if (mo.Message.Contains("TEST"))
                            {
                                MyApp.Instance.CurrActivity.ShowShortToast("MESSAGE_WRITE: " + mo.Message);
                            }

                            Log.Info(MyApp.TAG, "MESSAGE_WRITE: " + mo.Message);

                            break;
                        }
                    case Constants.MESSAGE_READ:
                        {
                            var readBuffer = (byte[])msg.Obj;

                            //var readMessage = Encoding.ASCII.GetString(readBuffer);

                            //var text = $"MESSAGE_READ {readMessage}";
                            //MyApp.Instance.CurrActivity.ShowShortToast(text);
                            //Log.Info(MyApp.TAG, text);

                            var mo = (MessageObject)MyApp.Instance.BtService.ByteArrayToObject(readBuffer);

                            if (mo.Message.Contains("TEST"))
                            {
                                MyApp.Instance.CurrActivity.ShowShortToast("MESSAGE_READ: " + mo.Message);
                            }

                            Log.Info(MyApp.TAG, "MESSAGE_READ: " + mo.Message);

                            MyApp.Instance.CurrActivity.OnMessageObject(mo);

                            break;
                        }
                    case Constants.MESSAGE_DEVICE_NAME:
                        {
                            var text = $"MESSAGE_DEVICE_NAME {msg.Data.GetString(Constants.DEVICE_NAME)}.";
                            MyApp.Instance.CurrActivity.ShowShortToast(text);
                            Log.Info(MyApp.TAG, text);

                            break;
                        }
                    case Constants.MESSAGE_TOAST:
                        break;
                }
            }
            catch (Exception ex)
            {
                Log.Error(MyApp.TAG, ex.ToString());
            }
        }

    }
}