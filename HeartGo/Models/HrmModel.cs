﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace HeartGo.Models
{
    [Serializable]
    public class HrmModel : _Model
    {

        public HrmFlags Flags { get; set; }
        public int BeatsPerMinute { get; set; }
        public float OxygenLevel { get; set; }

    }

    [Flags]
    public enum HrmFlags
    {
        Off = 0x00,
        On = 0x01,
    }
}