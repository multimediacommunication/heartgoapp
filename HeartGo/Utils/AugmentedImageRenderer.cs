﻿using System;
using Android.Content;
using Android.Graphics;
using Android.Opengl;
using DE.Javagl.Obj;
using Google.AR.Core;
using Java.Nio;
//using JavaGl.Obj;

namespace HeartGo.Utils
{
    public class AugmentedImageRenderer 
    {

        const string TAG = "AUGMENTEDIMAGERENDERER";

        const float TINT_INTENSITY = 0.1f;
        const float TINT_ALPHA = 1.0f;
        static readonly int[] TINT_COLORS_HEX = {
            0x000000, 0xF44336, 0xE91E63, 0x9C27B0, 0x673AB7, 0x3F51B5, 0x2196F3, 0x03A9F4, 0x00BCD4,
            0x009688, 0x4CAF50, 0x8BC34A, 0xCDDC39, 0xFFEB3B, 0xFFC107, 0xFF9800,
        };

        private ObjectRenderer imageFrame = new ObjectRenderer();

        private float scaleFactor = 0.001f;
        private float scaleFactorIncPerc = 0.05f;

        public AugmentedImageRenderer()
        {
        
        }

        public void CreateOnGlThread(Context context)
        {
            imageFrame.CreateOnGlThread(context, "Heart_symbol.obj", "Heart_texture.png");
            imageFrame.setMaterialProperties(0.0f, 3.5f, 1.0f, 6.0f);
        }

        public void Draw(float[] viewMatrix, float[] projectionMatrix, AugmentedImage augmentedImage, Anchor centerAnchor, float[] colorCorrectionRgba, float lightIntensity, float increase)
        {
            float[] tintColor = ConvertHexToColor(TINT_COLORS_HEX[augmentedImage.Index % TINT_COLORS_HEX.Length]);

            Pose[] localBoundaryPoses = {
                Pose.MakeRotation(0, 0, 0, 0)
            };
           

            //var size = 4;
            var size = 1;
            Pose anchorPose = centerAnchor.Pose;
            Pose[] worldBoundaryPoses = new Pose[size];
            for (int i = 0; i < size; ++i)
            {
                worldBoundaryPoses[i] = anchorPose.Compose(localBoundaryPoses[i]);
            }

            //float scaleFactor = 0.001f;
            //float scaleFactor = scaleFactorBeatBool ? 0.001f : 0.002f;
            //scaleFactorBeatBool = !scaleFactorBeatBool;
            //scaleFactorBeat += 0.000001f;

            //if (increase > 0)
            //{
            //    scaleFactor += scaleFactor * scaleFactorIncPerc;
            //}
            //else if (increase < 0)
            //{
            //    scaleFactor -= scaleFactor * scaleFactorIncPerc;
            //}

            if (increase != 0)
            {
                scaleFactor += scaleFactor * scaleFactorIncPerc * increase;
            }

            float[] modelMatrix = new float[16];

            worldBoundaryPoses[0].ToMatrix(modelMatrix, 0);
            imageFrame.updateModelMatrix(modelMatrix, scaleFactor);
            imageFrame.Draw(viewMatrix, projectionMatrix, lightIntensity);

            //worldBoundaryPoses[0].ToMatrix(modelMatrix, 0);
            //imageFrameUpperLeft.updateModelMatrix(modelMatrix, scaleFactor);
            //imageFrameUpperLeft.draw(viewMatrix, projectionMatrix, colorCorrectionRgba, tintColor);

            //worldBoundaryPoses[1].toMatrix(modelMatrix, 0);
            //imageFrameUpperRight.updateModelMatrix(modelMatrix, scaleFactor);
            //imageFrameUpperRight.draw(viewMatrix, projectionMatrix, colorCorrectionRgba, tintColor);

            //worldBoundaryPoses[2].toMatrix(modelMatrix, 0);
            //imageFrameLowerRight.updateModelMatrix(modelMatrix, scaleFactor);
            //imageFrameLowerRight.draw(viewMatrix, projectionMatrix, colorCorrectionRgba, tintColor);

            //worldBoundaryPoses[3].toMatrix(modelMatrix, 0);
            //imageFrameLowerLeft.updateModelMatrix(modelMatrix, scaleFactor);
            //imageFrameLowerLeft.draw(viewMatrix, projectionMatrix, colorCorrectionRgba, tintColor);
        }

        private static float[] ConvertHexToColor(int colorHex)
        {
            float red = ((colorHex & 0xFF0000) >> 16) / 255.0f * TINT_INTENSITY;
            float green = ((colorHex & 0x00FF00) >> 8) / 255.0f * TINT_INTENSITY;
            float blue = (colorHex & 0x0000FF) / 255.0f * TINT_INTENSITY;
            return new float[] { red, green, blue, TINT_ALPHA };
        }

    }
}