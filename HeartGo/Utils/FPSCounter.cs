﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace HeartGo.Utils
{
    public class FPSCounter
    {
        long startTime = NanoTime();
        int frames = 0;

        public void LogFrame()
        {
            frames++;
            if (NanoTime() - startTime >= 1000000000)
            {
                Log.Info("FPSCounter", "fps: " + frames);
                frames = 0;
                startTime = NanoTime();
            }
        }

        private static long NanoTime()
        {
            long nano = 10000L * Stopwatch.GetTimestamp();
            nano /= TimeSpan.TicksPerMillisecond;
            nano *= 100L;
            return nano;
        }
    }
}