﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using HeartGo.Activities;
using HeartGo.Bluetooth;

namespace HeartGo
{
    public class MyApp : Application
    {

        public static string TAG = "HeartGo";

        public static MyApp Instance;
        static MyApp() { Instance = new MyApp(); }
        private MyApp() { }

        /// <summary>
        /// Current Activity
        /// </summary>
        public _Activity CurrActivity { get; set; }

        /// <summary>
        /// BluetoothService to send messages
        /// </summary>
        public BluetoothService BtService { get; set; }

        /// <summary>
        /// Handles the messages for the activities
        /// </summary>
        public MessageHandler BtMessageHandler { get; set; }

        /// <summary>
        /// Selected device to send info
        /// </summary>
        public BluetoothDevice BtSelectedDevice { get; set; }

    }
}