﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HeartGo.Bluetooth;

namespace HeartGo.Activities
{
    [Activity(Theme = "@style/Theme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : _Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            SetContentView(Resource.Layout.activity_splash);
        }

        protected override void OnResume()
        {
            base.OnResume();
            var startup = new Task(() => { StartUp(); });
            startup.Start();
        }

        async void StartUp()
        {
            RunOnUiThread(() =>
            {
                var handler = new MessageHandler();
                MyApp.Instance.BtService = new BluetoothService(handler);
                MyApp.Instance.BtService.Start();
            });

            await Task.Delay(3000); // Simulate a bit of startup work.
            StartActivity(typeof(MainActivity));
        }
    }
}