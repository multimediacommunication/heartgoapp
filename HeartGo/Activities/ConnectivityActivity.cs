﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using HeartGo.Bluetooth;
using HeartGo.Models;

namespace HeartGo.Activities
{
    [Activity(Label = "Connectivity")]
    public class ConnectivityActivity : _Activity
    {

        private ImageView ImgBt;
        private TextView TxtSelectedDevice;
        private ListView LstPairedDevices;
        private Button BtnRestartService;
        private Button BtnConnect;
        private Button BtnTest;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_connectivity);
            ImgBt = FindViewById<ImageView>(Resource.Id.img_bt);
            TxtSelectedDevice = FindViewById<TextView>(Resource.Id.txt_selected_device);
            LstPairedDevices = FindViewById<ListView>(Resource.Id.lst_paired_devices);
            BtnRestartService = FindViewById<Button>(Resource.Id.btn_restart_service);
            BtnConnect = FindViewById<Button>(Resource.Id.btn_connect);
            BtnTest = FindViewById<Button>(Resource.Id.btn_test);

            BtnRestartService.Click += BtnRestartService_Click;
            BtnConnect.Click += BtnConnect_Click;
            BtnTest.Click += BtnTest_Click;

            var pairedDevicesArrayAdapter = new ArrayAdapter<string>(this, Resource.Layout.device_name);
            LstPairedDevices.Adapter = pairedDevicesArrayAdapter;
            LstPairedDevices.ItemClick += LstPairedDevices_ItemClick;

            var pairedDevices = BluetoothAdapter.DefaultAdapter.BondedDevices;
            if (pairedDevices.Count > 0)
            {
                foreach (var device in pairedDevices)
                {
                    pairedDevicesArrayAdapter.Add(device.Name + "\n" + device.Address);
                }
            }
            else
            {
                pairedDevicesArrayAdapter.Add("No devices have been paired");
            }

            SetSelectedDevice(MyApp.Instance.BtSelectedDevice);
        }

        protected override void OnResume()
        {
            base.OnResume();

            //if (MyApp.Instance.BtService != null)
            //{
            //    if (MyApp.Instance.BtService.GetState() == Bluetooth.BluetoothService.STATE_NONE)
            //    {
            //        MyApp.Instance.BtService.Start();
            //    }
            //}
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            //if (MyApp.Instance.BtService != null)
            //{
            //    MyApp.Instance.BtService.Stop();
            //}
        }

        private void BtnRestartService_Click(object sender, EventArgs e)
        {
            if (MyApp.Instance.BtService != null)
            {
                MyApp.Instance.BtSelectedDevice = null;
                MyApp.Instance.BtService.Stop();
                MyApp.Instance.BtService.Start();

            }
        }

        private void BtnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                if (MyApp.Instance.BtSelectedDevice != null)
                {
                    if (MyApp.Instance.BtService.GetState() != Bluetooth.BluetoothService.STATE_CONNECTED)
                    {
                        MyApp.Instance.BtService.Connect(MyApp.Instance.BtSelectedDevice, true);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowShortToast("Error: " + ex.Message);
                Log.Error(MyApp.TAG, ex.ToString());
            }
        }

        private void BtnTest_Click(object sender, EventArgs e)
        {
            try
            {
                if (MyApp.Instance.BtSelectedDevice != null)
                {
                    if (MyApp.Instance.BtService.GetState() == Bluetooth.BluetoothService.STATE_CONNECTED)
                    {
                        //var mo = new MessageObject<int>()
                        //{
                        //    Message = "TEST AT " + DateTime.Now.ToString("HH:mm:ss"),
                        //    Data = 100
                        //};

                        var mo = new MessageObject<HrmModel>()
                        {
                            Message = "TEST AT " + DateTime.Now.ToString("HH:mm:ss"),
                            Data = new HrmModel()
                            {
                                BeatsPerMinute = 80
                            }
                        };

                        MyApp.Instance.BtService.SendMessageObject(mo);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowShortToast("Error: " + ex.Message);
                Log.Error(MyApp.TAG, ex.ToString());
            }
        }

        private void LstPairedDevices_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            // Get the device MAC address, which is the last 17 chars in the View
            var info = ((TextView)e.View).Text;
            var address = info.Substring(info.Length - 17);

            var pairedDevices = BluetoothAdapter.DefaultAdapter.BondedDevices;
            if (pairedDevices.Count > 0)
            {
                foreach (var device in pairedDevices)
                {
                    if (device.Address == address)
                    {
                        SetSelectedDevice(device);
                    }
                }
            }
        }

        private void SetSelectedDevice (BluetoothDevice device)
        {
            if (device != null)
            {
                MyApp.Instance.BtSelectedDevice = device;
                TxtSelectedDevice.Text = device.Name + "\n" + device.Address;
                ImgBt.SetImageResource(Resource.Drawable.ic_bluetooth);
            }
        }
    }
}