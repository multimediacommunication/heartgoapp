﻿using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Widget;
using HeartGo.Activities;

namespace HeartGo
{
    [Activity(Label = "@string/app_name")]
    public class MainActivity : _Activity
    {

        LinearLayout LayConnectivity;
        LinearLayout LayReader;
        LinearLayout LayAr;
        LinearLayout LayCredits;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            SetContentView(Resource.Layout.activity_main);

            LayConnectivity = FindViewById<LinearLayout>(Resource.Id.lay_connectivity);
            LayReader = FindViewById<LinearLayout>(Resource.Id.lay_reader);
            LayAr = FindViewById<LinearLayout>(Resource.Id.lay_ar);
            LayCredits = FindViewById<LinearLayout>(Resource.Id.lay_credits);

            LayConnectivity.Click += LayConnectivity_Click;
            LayReader.Click += LayReader_Click;
            LayAr.Click += LayAr_Click;
            LayCredits.Click += LayCredits_Click;
        }

        protected override void OnResume()
        {
            base.OnResume();

            if (!CheckPermission(Android.Manifest.Permission.Camera))
            {
                RequestPermission(Android.Manifest.Permission.Camera);
            }

            if (!CheckPermission(Android.Manifest.Permission.BodySensors))
            {
                RequestPermission(Android.Manifest.Permission.BodySensors);
            }

            if (!CheckPermission(Android.Manifest.Permission.AccessFineLocation))
            {
                RequestPermission(Android.Manifest.Permission.AccessFineLocation);
            }
        }

        private void LayConnectivity_Click(object sender, System.EventArgs e)
        {
            StartActivity(typeof(ConnectivityActivity));
        }

        private void LayReader_Click(object sender, System.EventArgs e)
        {
            StartActivity(typeof(ReaderActivity));
        }

        private void LayAr_Click(object sender, System.EventArgs e)
        {
            StartActivity(typeof(ArActivity));
        }

        private void LayCredits_Click(object sender, System.EventArgs e)
        {
            StartActivity(typeof(CreditsActivity));
        }
    }
}