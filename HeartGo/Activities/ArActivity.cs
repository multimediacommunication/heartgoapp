﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Android.App;
using Android.Content;
using Android.Media;
using Android.Opengl;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Support.V7.App;
using Android.Util;
using Android.Views;
using Android.Widget;

using Google.AR.Core;
using Google.AR.Core.Exceptions;
using HeartGo.Bluetooth;
using HeartGo.Models;
using HeartGo.Utils;
using Javax.Microedition.Khronos.Egl;
using Javax.Microedition.Khronos.Opengles;
using Config = Google.AR.Core.Config;

namespace HeartGo.Activities
{
    [Activity(Label = "A.R.")]
    public class ArActivity : _Activity, GLSurfaceView.IRenderer, View.IOnTouchListener
    {

        Session session;
        LinearLayout layContent;
        GLSurfaceView surfaceView;
        Snackbar snackbar = null;

        TextView txtHr, txtOxygen;

        DisplayRotationHelper displayRotationHelper;
        BackgroundRenderer backgroundRenderer = new BackgroundRenderer();
        AugmentedImageRenderer augmentedImageRenderer = new AugmentedImageRenderer();

        Dictionary<int, Tuple<AugmentedImage, Anchor>> augmentedImageMap = new Dictionary<int, Tuple<AugmentedImage, Anchor>>();

        FPSCounter fpsCounter;

        protected float beatsPerMinute;
        protected float msPerBeat;
        protected float oxygenLevel;

        Beater beater;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_ar);
            layContent = FindViewById<LinearLayout>(Resource.Id.lay_content);
            txtHr = FindViewById<TextView>(Resource.Id.txt_hr);
            txtOxygen = FindViewById<TextView>(Resource.Id.txt_oxygen);
            surfaceView = FindViewById<GLSurfaceView>(Resource.Id.surfaceview);
            displayRotationHelper = new DisplayRotationHelper(this);

            try
            {
                session = new Session(this);
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, ex.Message, ToastLength.Long).Show();
                return;
            }

            var inputStream = Assets.Open("myimages.imgdb");
            var imageDatabase = AugmentedImageDatabase.Deserialize(session, inputStream);

            var config = new Config(session);
            config.AugmentedImageDatabase = imageDatabase;
            session.Configure(config);
            
            // Set up renderer.
            surfaceView.PreserveEGLContextOnPause = true;
            surfaceView.SetEGLContextClientVersion(2);
            surfaceView.SetEGLConfigChooser(8, 8, 8, 8, 16, 0); // Alpha used for plane blending.
            surfaceView.SetRenderer(this);
            surfaceView.RenderMode = Rendermode.Continuously;

            beater = new Beater(this);
            SetInfo(0, 0f);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            beater.Stop();
        }

        protected override void OnResume()
        {
            base.OnResume();

            // ARCore requires camera permissions to operate. If we did not yet obtain runtime
            // permission on Android M and above, now is a good time to ask the user for it.
            if (CheckPermission(Android.Manifest.Permission.Camera))
            {
                if (session != null)
                {
                    ShowSnackbar("Searching for hearts...");
                    // Note that order matters - see the note in onPause(), the reverse applies here.
                    session.Resume();
                }

                surfaceView.OnResume();
                displayRotationHelper.OnResume();
            }
            else
            {
                RequestPermission(Android.Manifest.Permission.Camera);
                //OnResume(); // Check Again
            }
        }

        protected override void OnPause()
        {
            base.OnPause();

            displayRotationHelper.OnPause();
            surfaceView.OnPause();
            if (session != null)
            {
                session.Pause();
            }
        }

        public override void OnWindowFocusChanged(bool hasFocus)
        {
            base.OnWindowFocusChanged(hasFocus);

            if (hasFocus)
            {
                // Standard Android full-screen functionality.
                //Window.DecorView.SystemUiVisibility = Android.Views.SystemUiFlags.LayoutStable
                //| Android.Views.SystemUiFlags.LayoutHideNavigation
                //| Android.Views.SystemUiFlags.LayoutFullscreen
                //| Android.Views.SystemUiFlags.HideNavigation
                //| Android.Views.SystemUiFlags.Fullscreen
                //| Android.Views.SystemUiFlags.ImmersiveSticky;

                Window.AddFlags(WindowManagerFlags.KeepScreenOn);
            }
        }

        public void OnSurfaceCreated(IGL10 gl, Javax.Microedition.Khronos.Egl.EGLConfig config)
        {
            try
            {
                GLES20.GlClearColor(0.1f, 0.1f, 0.1f, 1.0f);
                backgroundRenderer.CreateOnGlThread(this);
                augmentedImageRenderer.CreateOnGlThread(this);
            }
            catch (Exception ex)
            {
                ShowShortToast("Error: " + ex.Message);
                Log.Error(MyApp.TAG, ex.ToString());
            }
        }

        public void OnSurfaceChanged(IGL10 gl, int width, int height)
        {
            try
            {
                displayRotationHelper.OnSurfaceChanged(width, height);
                GLES20.GlViewport(0, 0, width, height);
            }
            catch (Exception ex)
            {
                ShowShortToast("Error: " + ex.Message);
                Log.Error(MyApp.TAG, ex.ToString());
            }
        }

        public void OnDrawFrame(IGL10 gl)
        {
            try
            {
                // Clear screen to notify driver it should not load any pixels from previous frame.
                GLES20.GlClear(GLES20.GlColorBufferBit | GLES20.GlDepthBufferBit);

                if (session == null)
                {
                    return;
                }

                if (fpsCounter == null)
                {
                    fpsCounter = new FPSCounter();
                }

                // Notify ARCore session that the view size changed so that the perspective matrix and the video background
                // can be properly adjusted
                displayRotationHelper.UpdateSessionIfNeeded(session);

                session.SetCameraTextureName(backgroundRenderer.TextureId);

                // Obtain the current frame from ARSession. When the configuration is set to
                // UpdateMode.BLOCKING (it is by default), this will throttle the rendering to the
                // camera framerate.
                Frame frame = session.Update();
                Camera camera = frame.Camera;

                backgroundRenderer.Draw(frame);

                // If not tracking, don't draw 3d objects.
                if (camera.TrackingState == TrackingState.Paused)
                {
                    return;
                }

                // Get projection matrix.
                float[] projmtx = new float[16];
                camera.GetProjectionMatrix(projmtx, 0, 0.1f, 100.0f);

                // Get camera matrix and draw.
                float[] viewmtx = new float[16];
                camera.GetViewMatrix(viewmtx, 0);

                // Compute lighting from average intensity of the image.
                float[] colorCorrectionRgba = new float[4];
                frame.LightEstimate.GetColorCorrection(colorCorrectionRgba, 0);
                float lightIntensity = frame.LightEstimate.PixelIntensity;

                DrawAugmentedImages(frame, projmtx, viewmtx, colorCorrectionRgba, lightIntensity);

                return;
            }
            catch (System.Exception ex)
            {
                // Avoid crashing the application due to unhandled exceptions.
                Log.Error(MyApp.TAG, "Exception on the OpenGL thread", ex);
            }
        }

        public void DrawAugmentedImages(Frame frame, float[] projmtx, float[] viewmtx, float[] colorCorrectionRgba, float lightIntensity)
        {
            System.Collections.ICollection updatedAugmentedImages =  frame.GetUpdatedTrackables(Java.Lang.Class.FromType(typeof(AugmentedImage)));

            // Iterate to update augmentedImageMap, remove elements we cannot draw.
            foreach (AugmentedImage augmentedImage in updatedAugmentedImages)
            {
                if (augmentedImage.TrackingState == TrackingState.Paused)
                {
                    // When an image is in PAUSED state, but the camera is not PAUSED, it has been detected,
                    // but not yet tracked.
                    String text = String.Format("Detected Image {0}", augmentedImage.Index);
                    ShowSnackbar(text);
                    Log.Info(MyApp.TAG, text);
                    break;
                }
                else if (augmentedImage.TrackingState == TrackingState.Tracking)
                {
                    // Create a new anchor for newly found images.
                    if (!augmentedImageMap.ContainsKey(augmentedImage.Index))
                    {
                        Anchor centerPoseAnchor = augmentedImage.CreateAnchor(augmentedImage.CenterPose);
                        augmentedImageMap.Add(augmentedImage.Index, Tuple.Create(augmentedImage, centerPoseAnchor));
                    }

                    HideSnackbar();
                }
                else if (augmentedImage.TrackingState == TrackingState.Stopped)
                {
                    augmentedImageMap.Remove(augmentedImage.Index);

                    ShowSnackbar("Searching for hearts...");
                }
            }

            var increase = beater.GetBeatIncrease();

            // Draw all images in augmentedImageMap
            foreach (var tuple in augmentedImageMap.Values)
            {
                AugmentedImage augmentedImage = tuple.Item1;
                Anchor centerAnchor = augmentedImageMap.GetValueOrDefault(augmentedImage.Index).Item2;
                if (augmentedImage.TrackingState == TrackingState.Tracking)
                {
                    augmentedImageRenderer.Draw(viewmtx, projmtx, augmentedImage, centerAnchor, colorCorrectionRgba, lightIntensity, increase);
                }
            }
        }

        public bool OnTouch(View v, MotionEvent e)
        {
            //throw new NotImplementedException();
            return true;
        }

        public override void OnMessageObject(MessageObject messageObject)
        {
            base.OnMessageObject(messageObject);

            ShowShortToast(messageObject.Message);

            var mo = (MessageObject<HrmModel>)messageObject;
            SetInfo(mo.Data.BeatsPerMinute, mo.Data.OxygenLevel);
        }

        private void ShowSnackbar(string msg)
        {
            if (snackbar == null)
            {
                RunOnUiThread(() =>
                {
                    try
                    {
                        snackbar = Snackbar.Make(layContent, msg, Snackbar.LengthIndefinite);
                        snackbar.View.SetBackgroundColor(Android.Graphics.Color.DarkGray);
                        snackbar.Show();
                    }
                    catch (Exception ex)
                    {
                        ShowShortToast("Error: " + ex.Message);
                        Log.Error(MyApp.TAG, ex.ToString());
                    }
                });
            }
        }

        private void HideSnackbar()
        {
            if (snackbar != null)
            {
                RunOnUiThread(() =>
                {

                    snackbar.Dismiss();
                    snackbar = null;

                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bpm">beatsPerMinute</param>
        /// <param name="ol">oxygenLevel</param>
        private void SetInfo(float bpm, float ol)
        {
            beatsPerMinute = bpm;
            oxygenLevel = ol;

            txtHr.Text = string.Format("{0} BPM", beatsPerMinute);
            txtOxygen.Text = string.Format("{0:0} %", oxygenLevel * 100);

            if (beater != null)
            {
                beater.Stop();
            }

            if (bpm > 0)
            {
                msPerBeat = 1.0f / (bpm / 60000.0f);

                if (beater != null)
                {
                    beater.Start();
                }
            }
        }

        public class Beater
        {
            ArActivity activity;
            Timer timer;
            long[] vibrationPattern;
            Vibrator vibrator;
            VibrationEffect vibrationEffect;
            MediaPlayer mediaPlayer;
            PlaybackParams playbackParams;
            bool mediaPlayerPrepared;
            float speed;

            long msWait = -1;
            long msIncrease = -1;
            long msWaitBig = -1;
            long msDecrease = -1;

            public DateTime TimeOfBeat { get; set; }

            int i = 0;

            public Beater(ArActivity activity)
            {

                try
                {
                    this.activity = activity;
                    
                    vibrator = (Vibrator)activity.GetSystemService(VibratorService);

                    speed = activity.beatsPerMinute / 60.0f;
                    playbackParams = new PlaybackParams();
                    playbackParams.SetSpeed(speed);
                    playbackParams.SetPitch(speed);

                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.SetDataSource(activity.Assets.OpenFd("Heartbeat.mp3"));
                    //mediaPlayer.PlaybackParams = playbackParams;
                    mediaPlayer.Looping = false;

                    mediaPlayer.Prepared += MediaPlayer_Prepared;
                    mediaPlayer.Prepare();

                }
                catch (Exception ex)
                {
                    Log.Error(MyApp.TAG, ex.ToString());
                }
            }

            private void MediaPlayer_Prepared(object sender, EventArgs e)
            {
                mediaPlayerPrepared = true;
            }

            public void Start()
            {
                vibrationPattern = new long[]
                {
                    (long)(activity.msPerBeat * 0.160) + 80,
                    (long)(activity.msPerBeat * 0.090),
                    (long)(activity.msPerBeat * 0.240),
                    (long)(activity.msPerBeat * 0.090),
                };

                vibrationEffect = VibrationEffect.CreateWaveform(vibrationPattern, -1);

                msWait = vibrationPattern[0];
                msIncrease = msWait + vibrationPattern[1];
                msWaitBig = msIncrease + vibrationPattern[2];
                msDecrease = msWaitBig + vibrationPattern[3];

                speed = activity.beatsPerMinute / 60.0f;
                playbackParams = new PlaybackParams();
                playbackParams.SetSpeed(speed);
                playbackParams.SetPitch(speed);

                //mediaPlayer.SetDataSource(activity.Assets.OpenFd("Heartbeat.mp3"));
                mediaPlayer.PlaybackParams = playbackParams;
                //mediaPlayer.Looping = false;
                //mediaPlayer.Prepare();

                TimeOfBeat = DateTime.MinValue;
                var autoEvent = new AutoResetEvent(false);
                timer = new Timer(Beat, autoEvent, 0, (int)activity.msPerBeat);
            }

            public void Stop()
            {
                if (timer != null)
                {
                    timer.Dispose();
                    timer = null;

                    //mediaPlayer.Stop();
                    //mediaPlayer.Dispose();
                    //mediaPlayer.SetDataSource(activity.Assets.OpenFd("Heartbeat.mp3"));
                    //mediaPlayer.PlaybackParams = playbackParams;
                    //mediaPlayer.Looping = false;
                    //mediaPlayerPrepared = false;
                }
            }

            public void Beat(object stateInfo)
            {
                try
                {
                    Log.Info(MyApp.TAG, "Beat! " + mediaPlayer.Duration);

                    if (mediaPlayerPrepared && mediaPlayer != null)
                    {
                        TimeOfBeat = DateTime.Now;
                        if (!mediaPlayer.IsPlaying)
                        {
                            mediaPlayer.Start();
                        }
                        else
                        {
                            mediaPlayer.SeekTo(0);
                            //mediaPlayer.SeekTo((int)(32 / speed));
                        }
                        vibrator.Vibrate(vibrationEffect);
                    }

                    //if (i == 5)
                    //{
                    //    activity.SetBeats(120);
                    //}
                    //i++;

                }
                catch (Exception ex)
                {
                    Log.Error(MyApp.TAG, ex.ToString());
                }
            }

            public float GetBeatIncrease()
            {
                var increase = 0f;
                var ms = DateTime.Now.Subtract(TimeOfBeat).Milliseconds;
                if (ms > msWait && ms < msIncrease)
                {
                    increase = 1f * speed;
                }
                else if (ms > msWaitBig && ms < msDecrease)
                {
                    increase = -1f * speed;
                }

                return increase;
            }

        }

    }
}