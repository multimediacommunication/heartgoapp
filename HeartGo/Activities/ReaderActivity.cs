﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using Com.Samsung.Android.Sdk;
using Com.Samsung.Android.Sdk.Sensorextension;

using Android.Support.Design.Widget;
using HeartGo.Models;
using HeartGo.Bluetooth;

namespace HeartGo.Activities
{
    [Activity(Label = "Reader")]
    public class ReaderActivity : _Activity
    {
        Ssensor ir = null;
        Ssensor red = null;

        //private final static int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 101;
        ToggleButton btn_start = null;
        TextView tSD = null;
        TextView tResults = null;

        //Calculo de pulso y Oxigeno
        List<float> senalIr = null;
        List<float> senalRed = null;
        List<float> senalIrS = null;
        long inicio = 0;
        HrmModel hrmModel = new HrmModel();

        SSListener mSSListener = null;

        SsensorManager mSSensorManager = null;
        public SsensorExtension mSsensorExtension = null;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_reader);

            tSD = FindViewById<TextView>(Resource.Id.tSensor);
            tResults = FindViewById<TextView>(Resource.Id.tResults);
            btn_start = FindViewById<ToggleButton>(Resource.Id.btn_start);

            if (btn_start != null)
            {
                btn_start.Click += delegate
                {
                    btn_start.Selected = !btn_start.Selected;
                    try
                    {
                        if (!btn_start.Selected)
                        {
                            // HRM OFF
                            btn_start.Text = btn_start.TextOff;
                            if (mSSensorManager != null)
                            {
                                if (ir != null)
                                    mSSensorManager.UnregisterListener(mSSListener, ir);
                                if (red != null)
                                    mSSensorManager.UnregisterListener(mSSListener, red);
                            }
                            CalcularStats();
                        }
                        else
                        {
                            try
                            {
                                mSsensorExtension = new SsensorExtension();
                                mSsensorExtension.Initialize(this);
                                senalIr = new List<float>();
                                senalRed = new List<float>();
                                senalIrS = new List<float>();
                                mSSListener = new SSListener(mSsensorExtension, tSD, senalIr, senalRed, senalIrS);
                                mSSensorManager = new SsensorManager(this, mSsensorExtension);
                                ir = mSSensorManager.GetDefaultSensor(Ssensor.TypeHrmLedIr);
                                red = mSSensorManager.GetDefaultSensor(Ssensor.TypeHrmLedRed);
                            }
                            catch (Exception e)
                            {
                                ShowShortToast(e.Message);
                                this.Finish();
                            }
                            // HRM ON
                            btn_start.Text = btn_start.TextOn;
                            inicio = Java.Lang.JavaSystem.CurrentTimeMillis();
                            hrmModel.Flags = HrmFlags.On;
                            hrmModel.OxygenLevel = 0;
                            hrmModel.BeatsPerMinute = 0;
                            SendMessage(hrmModel);
                            if (mSSensorManager != null)
                            {
                                if (ir != null)
                                    mSSensorManager.RegisterListener(mSSListener, ir, (int)Android.Hardware.SensorDelay.Normal);
                                if (red != null)
                                    mSSensorManager.RegisterListener(mSSListener, red, (int)Android.Hardware.SensorDelay.Normal);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        ShowShortToast(e.Message);
                    }
                };
            }
        }

        private void SendMessage(HrmModel model)
        {
            try
            {
                if (MyApp.Instance.BtSelectedDevice != null)
                {
                    if (MyApp.Instance.BtService.GetState() == Bluetooth.BluetoothService.STATE_CONNECTED)
                    {
                        var mo = new MessageObject<HrmModel>()
                        {
                            Message = "TEST AT " + DateTime.Now.ToString("HH:mm:ss"),
                            Data = model
                        };
                        MyApp.Instance.BtService.SendMessageObject(mo);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowShortToast("Error: " + ex.Message);
                Log.Error(MyApp.TAG, ex.ToString());
            }
        }

        private void CalcularStats()
        {
            long pulso = (60 / ((Java.Lang.JavaSystem.CurrentTimeMillis() - inicio) / 1000)) * ContarPicos(senalIrS).Count;
            float spo2 = senalIr.Sum() / senalRed.Sum(); //ContarPicos(senalIr).Sum() / ContarPicos(senalRed).Sum();
            tResults.Text = "HR: " + pulso.ToString() + "\nSpO2: " + spo2.ToString();
            hrmModel.BeatsPerMinute = (int)pulso;
            hrmModel.OxygenLevel = spo2;
            hrmModel.Flags = HrmFlags.Off;
            SendMessage(hrmModel);
            senalIr.Clear();
            senalIr = null;
            senalRed.Clear();
            senalRed = null;
            senalIrS.Clear();
            senalIrS = null;
        }

        private List<float> ContarPicos(List<float> senal)
        {
            List<float> picos = new List<float>();
            if (senal.Count > 500)
            {
                for (int i = 0; i < senal.Count; i++)
                {
                    if (i == 0 && senal[i] > senal[i + 1])
                    {
                        picos.Add(senal[i]);
                    }
                    if ((i > 0 && i < senal.Count - 1) && (senal[i] >= senal[i - 1] && senal[i] > senal[i + 1]))
                    {
                        picos.Add(senal[i]);
                    }
                    if (i == senal.Count - 1 && senal[i] >= senal[i - 1])
                    {
                        picos.Add(senal[i]);
                    }
                }
            }
            return picos;
        }

        class SSListener : Java.Lang.Object, ISsensorEventListener
        {
            private SsensorExtension mSsensorExtension;
            private TextView tSD = null;

            //Procesar la señal
            private float[] ventanaIR = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            List<float> senalIr = null;
            List<float> senalRed = null;
            List<float> senalIrS = null;
            float irValue = 0;
            float redValue = 0;

            public SSListener(SsensorExtension mSsensorExtension, TextView tSD, List<float> senalIr, List<float> senalRed, List<float> senalIrS)
            {
                this.mSsensorExtension = mSsensorExtension;
                this.tSD = tSD;
                this.senalIr = senalIr;
                this.senalRed = senalRed;
                this.senalIrS = senalIrS;
            }

            public void OnAccuracyChanged(Ssensor p0, int p1)
            {
                throw new NotImplementedException();
            }

            private float[] ShiftLeft(float[] ventana)
            {
                for (int i = 0; i < ventana.Length - 1; i++)
                {
                    ventana[i] = ventana[i + 1];
                }
                return ventana;
            }

            public void OnSensorChanged(SsensorEvent p0)
            {
                Ssensor s = p0.Sensor;
                string sb = null;
                sb = "==== Sensor Information ====\n";
                sb += "Vendor : " + s.Vendor + "\n";
                sb += "SDK Version : " + mSsensorExtension.VersionName + "\n";
                sb += "MaxRange : " + s.MaxRange + "\n";
                sb += "Resolution : " + s.Resolution + "\n";
                sb += "Power : " + s.Power + "\n";
                sb += "----------------------------\n";
                switch (s.Type)
                {
                    case Ssensor.TypeHrmLedIr:
                        irValue = p0.Values[0];
                        ShiftLeft(ventanaIR);
                        ventanaIR[ventanaIR.Length - 1] = irValue;
                        senalIrS.Add(ventanaIR.Average());
                        senalIr.Add(524287 - irValue);
                        break;
                    case Ssensor.TypeHrmLedRed:
                        redValue = p0.Values[0];
                        senalRed.Add(524287 - redValue);
                        break;
                }
                sb += "IR RAW DATA : " + irValue + "\n";
                sb += "RED RAW DATA : " + redValue + "\n";
                tSD.Text = sb;
            }
        }
    }
}