﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Support.V7.App;
using Android.Util;
using Android.Views;
using Android.Widget;
using HeartGo.Bluetooth;

namespace HeartGo.Activities
{
    [Activity]
    public abstract class _Activity : AppCompatActivity
    {

        public _Activity()
        {
            MyApp.Instance.CurrActivity = this;
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public bool CheckPermission(string permission)
        {
            var result = false;
            try
            {
                result = ContextCompat.CheckSelfPermission(this, permission) == Android.Content.PM.Permission.Granted;
            }
            catch (Exception ex)
            {
                ShowShortToast(ex.Message);
                Log.Error(MyApp.TAG, ex.ToString());
            }
            return result;
        }

        public void RequestPermission(string permission)
        {
            ActivityCompat.RequestPermissions(this, new string[] { permission }, 0);
        }

        public void ShowShortToast(string msg)
        {
            RunOnUiThread(() =>
            {
                Toast.MakeText(this, msg, ToastLength.Short).Show();
            });
        }

        public virtual void OnMessageObject(MessageObject messageObject)
        {

        }

        //TODO: Create events for handling messages
    }
}